try:
  from setuptools import setup
except ImportError:
  from distutils.core import setup

config = {
  'description': 'Proyecto1',
  'author': 'l4sh',
  'url': 'http://example.org',
  'download_url': 'http://example.org',
  'author_email': 'luis.salazar@uakami.com',
  'version': '0.1',
  'install_requires': ['nose'],
  'packages': ['NAME'],
  'scripts': [],
  'name': 'proyecto1'
}

setup(**config)
