# http://learnpythonthehardway.org/book/ex5.html
# more variables and printing

name = 'Juan'
age = 30
height = 180 # cm
weight = 90 # kg
eyes = 'blue'
hair = 'brown'

print "Let's talk about %s." % name
print "He's %d cm tall." % age
print "He weights %d Kg." % weight
print "He's got %s eyes and %s hair." % (eyes, hair)

print "If I add %d, %d, and %d I get %d." % (
    age, height, weight, age + height + weight)
