try:
  from setuptools import setup
except ImportError:
  from distutils.core import setup

config = {
  'description': 'ex48',
  'author': 'l4sh',
  'url': 'http://example.org',
  'download_url': 'http://example.org',
  'author_email': 'luis.salazar@uakami.com',
  'version': '0.1',
  'install_requires': ['nose'],
  'packages': ['ex48'],
  'scripts': [],
  'name': 'ex48'
}

setup(**config)
