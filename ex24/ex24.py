# http://learnpythonthehardway.org/book/ex24.html
# more practice

print "Practica"
print "\tEscape\n\'\nCharacters"

multiline = '''
\t uno
dos\ntres
cuatro\n\tcinco
'''

print '-------------------'
print multiline
print '*******************'

cinco = 10 - 2 + 3 - 6
print 'cinco: %s' % cinco

def secret_formula(started):
  jelly_beans = started * 500
  jars = jelly_beans / 1000
  crates = jars / 100
  return jelly_beans, jars, crates

start_point = 10000
beans, jars, crates = secret_formula(start_point)

print 'start point: %d' % start_point
print 'beans %d, jars %d, crates %d' % (beans, jars, crates)

start_point = start_point / 10

print 'b %d, j %d, c %d' % secret_formula(start_point)

