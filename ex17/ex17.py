# http://learnpythonthehardway.org/book/ex17.html
# more files

from sys import argv
from os.path import exists
script, from_file, to_file = argv

print 'copiando %s a %s' % (from_file, to_file)

in_file = open(from_file)
in_data = in_file.read()

print 'the input file is %d bytes long' % len(in_data)

print 'output file exists? %r' % exists(to_file)

print 'return to continue, ctrl-c to abort'
raw_input()

# write to file
out_file = open(to_file, 'w')
out_file.write(in_data)

print 'file copied'
in_file.close()
out_file.close()
