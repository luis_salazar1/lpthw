import web
from gothonweb import map

urls = (
  '/game', 'GameEngine',
  '/', 'Index',
  '/index', 'Index'
)

app = web.application(urls, globals())

# little hack so that debug mode works with sessions
if web.config.get('_session') is None:
    store = web.session.DiskStore('sessions')
    session = web.session.Session(app, store,
                                  initializer={'room': None})
    web.config._session = session
else:
    session = web.config._session

render = web.template.render('templates/', base="layout")


class Index(object):
    def GET(self):
        # this is used to "setup" the session with starting values
        session.room = map.START
        web.seeother("/game")


class GameEngine(object):

    def GET(self):
        if session.room:
            return render.show_room(room=session.room)
        #else:
            # why is there here? do you need it?
         #   return render.you_died()

    def POST(self):
        form = web.input(action=None)

        if session.room and form.action:

            if '*' in session.room.paths:
                if form.action != session.room.number:
                    session.room.tries -= 1
                    session.room.placeholder = "Denied! %d" % session.room.tries
                else:
                    session.room = session.room.go(session.room.number)

                if session.room.tries == 0:
                    session.room = session.room.go('*')

            else:

                if form.action not in session.room.paths:
                    session.room.placeholder = 'Wrong action, try again'
                else:
                    session.room = session.room.go(form.action)

        web.seeother("/game")

if __name__ == "__main__":
    app.run()
