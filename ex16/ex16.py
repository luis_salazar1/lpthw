# http://learnpythonthehardway.org/book/ex16.html
# reading and writing files

from sys import argv

script, fname = argv

print 'delete %r' % fname
print 'CTRL-C to cancel, else hit RETURN'

raw_input('?')

print 'opening file'
target = open(fname,'w')

print 'truncating file'
target.truncate()

print 'input 3 lines'

line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print 'writing to file'

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print 'closing file'
target.close()
